<?php


require_once "../config/databse.php";
require_once "../helper/RequestHandler.php";
use OodoFideclub\RequestHandler;

$conn = new DatabseConnection();

$query = "SELECT 
    users.`category_id`,
    client_categories.`en` as category_name,
    users.`firstname`,
    users.`owner_phone`,
    store_config.value as address,
    users.`lastname`,
    users.id as id_client,
    users.`phone_company`,
    users.`company_name`,
    users.`country_code`,
    users.`subdomain`,
    users.`email`

    FROM `users` 
    LEFT JOIN `client_categories` on `users`.category_id = `client_categories`.id
    LEFT JOIN `store_config` on (`users`.id = `store_config`.user_id and `store_config`.name = 'store_address')
    WHERE users.is_deleted = 0
   
";
$query = $conn->executeQuery($query);

while ($client = mysqli_fetch_assoc($query))
{
    $client['country_code'] == '' ? $client['country_code']= 'DO' : $client['country_code'];
    $client['lastname'] == '' ? $client['lastname']= $client['firstname'] : $client['lastname'];

    $data = sanitize($client);
    $data = createJsonBodyFor($client);
    echo $data . "<br>";
    $odooRequestHandler = new OodoFideclub\RequestHandler\RequestHandler();
    print_r($odooRequestHandler->post('customers', $data));

    echo "<br><br>";
}

function sanitize($client)
{
    foreach ($client as $key => $value)
    {

        $client[$key] =  htmlspecialchars($value, ENT_QUOTES);
        $client[$key] = preg_replace('/[^A-Za-z0-9\-]/', '', $value);
        // $client[$key] = trim(preg_replace('/\s+/', ' ',  $value));
    }

    return $client;
}

function createJsonBodyFor($client)
{
    $data = "{
      \"jsonrpc\": \"2.0\",
      \"id\": ".$client['id_client'].",			
      \"method\": \"call\",
      \"params\": {
        \"category\": [".$client['category_id'].", \"".$client['category_name']."\"],
        \"firstname\": \"".$client['firstname']."\",
        \"mobile\": \"".$client['owner_phone']."\",
        \"lastname\": \"".$client['lastname']."\",
        \"id\": ".$client['id_client'].",				
        \"phone\": \"".$client['phone_company']."\",
        \"company_name\": \"".$client['company_name']."\",
        \"country_code\": \"".$client['country_code']."\",
        \"address\": \"".$client['address']."\",
        \"subdomain\": \"".$client['subdomain']."\",
        \"salesman\": [
          [".$client['id_client'].", \"".$client['firstname']."\"]
        ],
        \"email\": \"".$client['email']."\",
        \"plan\": [10, \"Fideclub Plan\"]
      }
    }";

    return $data;
}